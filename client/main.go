package main

import (
	"context"
	"flag"
	"fmt"
	lddemopb "lddemo/proto"
	"log"
	"time"

	"github.com/bsm/grpclb/balancer"
	"github.com/bsm/grpclb/discovery/consul"
	"github.com/hashicorp/consul/api"
	"google.golang.org/grpc"
)

var flags struct {
	consul string
	name   string
}

var loadBalancer *balancer.Server

func init() {
	flag.StringVar(&flags.consul, "consul", "127.0.0.1:8500", "Consul API address. Default: 127.0.0.1:8500")
	flag.StringVar(&flags.name, "n", "hw", "Server name. Default: hw")
	initLB()
}

func initLB() {
	config := api.DefaultConfig()
	config.Address = flags.consul

	discovery, err := consul.New(config)
	if err != nil {
		panic(err)
	}

	loadBalancer = balancer.New(discovery, nil)
}

func main() {
	flag.Parse()
	for i := 0; i < 100000; i++ {
		server, err := getServer()
		if err != nil {
			log.Fatal("FATAL", err.Error())
		}
		if err = access(server); err != nil {
			log.Fatal("FATAL", err.Error())
		}
		time.Sleep(time.Second)
	}
}

func getServer() (string, error) {

	servers, err := loadBalancer.GetServers(flags.name)
	if err != nil {
		return "", err
	}
	if len(servers) == 0 {
		return "", fmt.Errorf("no server")
	}
	for _, server := range servers {
		fmt.Printf("%s\t%d\n", server.GetAddress(), server.GetScore())
	}
	return servers[0].GetAddress(), nil
}

func access(addr string) error {
	cc, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return err
	}
	defer cc.Close()
	hc := lddemopb.NewHelloWorldClient(cc)
	resp, err := hc.Say(context.Background(), &lddemopb.HelloRequest{
		Name: "Bob",
	})
	fmt.Printf("request: %s  recv:%s\n", addr, resp.GetEcho())
	return nil
}
