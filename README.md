# grpclb 包的使用

## 简述

* grpclb 是一个基于 grpc 和服务发现系统的负载均衡模块框架， 可以让用户自己接入服务发现模块，自己实现服务负载上报，以及自己实现负载均衡策略等
* grpclb 同时为上述功能提供了一个默认的支持，这个默认的支持基本满足大多数需求。


### grpclb 提供的功能

#### 服务发现

* grpclb 支持使用者自己接入服务发现模块，并且提供了以 consul 作为服务发现模块的支持
* 对应的源码在 grpclb/discovery 中。 用户只需要实现 balancer.Discovery 接口便可以创建自己的服务发现模块

#### 负载收集

* grpclb 根据 grpc 的 LoadReport 接口，从服务中收集其负载情况。所谓的负载在 grpclb 中表示为一个 Score (32位整形值)
* 服务需要实现 LoadReport 接口来提供负载拉取服务。 当然， 除此之外，服务也可以使用 grpclb 提供的现有接口来获取服务负载：
    * 基础上报: Reporter, 可以设置，增加，重置负载
    * 按时间指数衰减的负载上报：RateReporter， 其增加的负载值在一个特定时间后会清 0，在这个时间点之前，按照经过的时间逐步衰减。
* 如果服务有自己特有的需求，如根据 CPU 占用率、 IO 负载情况、房间服务器中的房间数量、甚至是这些的结合体， 则用户可以自己实现 LoadReport 接口

#### 均衡策略

* grpclb 可以让用户自己实现负载均衡策略，并且提供了现成的策略：
    * 随机策略： NewRandomBalancer， 将服务地址列表随机打乱排序，并且返回
    * 最不繁忙策略： NewLeastBusyBalancer， 将服务地址列表按照其负载值从低到高排序，并且返回
* 用户可以自己实现特有的负载均衡策略，如轮询策略、轮询+最不繁忙结合策略等

#### 用户接口

* grpclb 提供了根据服务名字获取服务地址的接口， 它是上述功能的综合， 它的流程：
    * grpclb 会根据接口调用传入的服务名字定时从服务发现中获取服务地址列表
    * grpclb 会根据获取到的服务地址列表， 定时收集服务是否健康，以及负载情况
    * grpclb 返回经过负载均衡策略所作的排序返回服务地址列表


### lddemo 

* lddemo 是基于 grpclb 包实现的一个简单的负载均衡 demo 
* lddemo 包括服务器、客户端以及通讯协议 3 个目录。 
* 服务器提供的是根据名字生成一个 echo 字符串的服务， 它会向 consul 注册自己，并且利用 grpclb 提供的 RateReporter 实现负载拉取服务。 
* 客户端会使用 grpclb 提供的负载均衡服务接口来获取服务地址，并且访问
