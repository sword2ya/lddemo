package main

import (
	"context"
	"flag"
	"fmt"
	lddemopb "lddemo/proto"
	"net"
	"strconv"
	"time"

	bckd "github.com/bsm/grpclb/grpclb_backend_v1"

	"github.com/hashicorp/consul/api"

	"github.com/Sirupsen/logrus"
	"github.com/bsm/grpclb/load"
	"github.com/go-redis/redis"

	"google.golang.org/grpc"
)

var flags struct {
	addr string
	port int
	name string
}
var reporter = load.NewRateReporter(5 * time.Second)

func init() {
	flag.StringVar(&flags.addr, "a", "127.0.0.1", "Bind address. Default: 127.0.0.1")
	flag.IntVar(&flags.port, "p", 6667, "Bind port. Default: 6666")
	flag.StringVar(&flags.name, "n", "hw", "Server Name. Default: hw")
}

type helloWorldServer struct{}

func (s *helloWorldServer) Say(ctx context.Context, req *lddemopb.HelloRequest) (*lddemopb.HelloResponse, error) {
	reporter.Increment(1)

	return &lddemopb.HelloResponse{
		Echo: "Hello, " + req.GetName(),
	}, nil
}

func main() {
	flag.Parse()
	svr := grpc.NewServer()
	lddemopb.RegisterHelloWorldServer(svr, &helloWorldServer{})
	bckd.RegisterLoadReportServer(svr, reporter)

	lis, err := net.Listen("tcp", fmt.Sprintf("%s:%d", flags.addr, flags.port))
	if err != nil {
		panic(err)
	}
	redisOpt := redis.Options{
		Addr: "127.0.0.1:6379",
	}
	if err := registerService(flags.name, flags.addr, flags.port, redis.NewClient(&redisOpt)); err != nil {
		panic(err)
	}
	svr.Serve(lis)
}

func allocServiceID(redisClient *redis.Client) (string, error) {
	result := redisClient.Incr("serviceloader:service:maxid")
	if result.Err() != nil {
		return "", fmt.Errorf("分配服务 ID 失败: %v", result.Err())
	}
	return strconv.FormatInt(result.Val(), 10), nil
}

func registerService(serviceName string, addr string, port int, redisClient *redis.Client) error {
	entry := logrus.WithFields(logrus.Fields{
		"service_name": serviceName,
		"address":      addr,
		"port":         port,
	})
	consul, err := api.NewClient(api.DefaultConfig())
	if err != nil {
		return fmt.Errorf("创建 consul 接口对象失败")
	}

	if serviceName == "" {
		entry.Warn("未配置服务名称，该服务不会被其他服务发现")
		return nil
	}
	serviceID, err := allocServiceID(redisClient)
	if err != nil {
		entry.WithError(err).Errorln("分配服务 ID 失败")
		return err
	}

	agent := consul.Agent()
	if err := agent.ServiceRegister(&api.AgentServiceRegistration{
		ID:      serviceID,
		Name:    serviceName,
		Tags:    []string{},
		Port:    port,
		Address: addr,
		// TODO: Checks
	}); err != nil {
		return fmt.Errorf("服务注册失败(%s, %s, %d)：%v", serviceName, addr, port, err)
	}
	return nil
}
